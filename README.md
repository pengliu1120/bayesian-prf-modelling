# Bayesian pRF modeling
Freely available for download from https://github.com/pzeidman/BayespRF

Associated literature https://www.sciencedirect.com/science/article/pii/S1053811917307462

# Modification
This repository contains modified pRF modeling script based on the purpose of investigating neural basis in human somatosensory cortex (area BA3b specifically).
