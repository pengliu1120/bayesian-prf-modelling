%% Run GLM analysis and extracts timeseries

% Settings

% Directory into dataset
data_root_dir = '/Users/pliu/Documents/DataAnalysis1219';
data_dir      = fullfile(data_root_dir,'Dataset','aae961');
surf_dir      = fullfile(data_root_dir,'Dataset','aae961','surf');
label_dir     = fullfile(data_root_dir,'Dataset','aae961','label');

% Directory for creating GLM
glm_dir  = fullfile(data_root_dir,'Results','aae961');

% Directory containing this script
script_dir = pwd;

% Number of sessions
nsess = 1; 

% Settings
TR            = 2;      % Repetition time
nmicrotime    = 16;     % Bins per TR
stim_duration = 5.12;   % Duration of stimuli (secs)
stim_diameter = 25;     % Diameter of stimuli in degrees
%% Set defaults
spm('defaults','FMRI')
spm_jobman('initcfg')
%% Prepare onsets
load(fullfile(data_dir,'aps_Bars.mat'));
U = prepare_inputs_polar_samsrf(ApFrm,TR,nmicrotime,stim_duration,stim_diameter); 

bins_x = [-12.5 -7.5 -2.5 2.5 7.5 12.5];
bins_y = 0;

% Build time x screen bins matrix (1x5 space bins)
onsets_matrix = zeros(length(U), length(bins_x) .^ 2);
for t = 1:length(U)
    % Loop over pixels activated at this time point
    for activated_pixel = 1:length(U(t).dist)
        % Get location
        dist  = U(t).dist(activated_pixel);
        angle = U(t).angle(activated_pixel);
        
        % Polar->cartesian
        x = dist * cos(angle);
        y = dist * sin(angle);

        % Identify closest bin
        [~,binx] = min(abs(bins_x-x));
        [~,biny] = min(abs(bins_y-y));
        
        % Binned coordintes -> index
        bin_idx = sub2ind([length(bins_x) length(bins_x)],biny,binx);

        onsets_matrix(t,bin_idx) = onsets_matrix(t,bin_idx) + 1;
    end
end

% Remove empty bins
onsets_matrix = onsets_matrix(:,any(onsets_matrix));
%num_regressors = size(onsets_matrix,2);
num_regressors = 5;

% SPM inputs
names = cell(1,num_regressors); 
onsets = cell(1,num_regressors); 
durations = cell(1,num_regressors); 

% Onsets
D1_1 = [0 : 25.6 : 511];
D1_2 = [532.48 : 25.6 : 1023];
D1 = [D1_1 D1_2];
onsets{1,1} = D1';

D2_1 = [(0+5.12) : 25.6 : 511];
D2_2 = [(532.48-5.12) : 25.6 : 1023];
D2 = [D2_1 D2_2];
onsets{1,2} = D2';

D3_1 = [(0+10.24) : 25.6 : 511];
D3_2 = [(532.48-10.24) : 25.6 : 1023];
D3 = [D3_1 D3_2];
onsets{1,3} = D3';

D4_1 = [(0+15.36) : 25.6 : 511];
D4_2 = [(532.48-15.36) : 25.6 : 1023];
D4 = [D4_1 D4_2];
onsets{1,4} = D4';

D5_1 = [(0+20.48) : 25.6 : 511];
D5_2 = [(532.48-20.48) : 25.6 : 1023];
D5 = [D5_1 D5_2];
onsets{1,5} = D5';

durations{1,1} = 5.12;
durations{1,2} = 5.12;
durations{1,3} = 5.12;
durations{1,4} = 5.12;
durations{1,5} = 5.12;

names{1,1} = 'Bin1';
names{1,2} = 'Bin2';
names{1,3} = 'Bin3';
names{1,4} = 'Bin4';
names{1,5} = 'Bin5';

save(fullfile(script_dir, 'onsets.mat'), 'names', 'onsets', 'durations');
%% Specify first level design

start_dir = pwd;

% Make output directory
if ~exist(glm_dir,'file')
    mkdir(glm_dir);
end

% Load generic matlabbatch for fmri_spec, fmri_est and con(trast)
load('aae961.mat');

% Session-specific options
%{
for i = 1:nsess
    movement = spm_select('FPList',data_dir,sprintf('rp_bfBars%d.txt',i));
    epis     = spm_select('ExtFPList',data_dir,sprintf('ubfBars%d.nii',i), 1:999);    
    
    matlabbatch{1}.spm.stats.fmri_spec.sess(i).multi_reg = cellstr(movement);
    matlabbatch{1}.spm.stats.fmri_spec.sess(i).scans     = cellstr(epis);
    matlabbatch{1}.spm.stats.fmri_spec.sess(i).multi     = cellstr('onsets.mat');
    
    matlabbatch{1}.spm.stats.fmri_spec.sess(i).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
    matlabbatch{1}.spm.stats.fmri_spec.sess(i).regress = struct('name', {}, 'val', {});
    matlabbatch{1}.spm.stats.fmri_spec.sess(i).hpf = 128;    
end
%}

% Model spec options
matlabbatch{1}.spm.stats.fmri_spec.dir = cellstr(glm_dir);
matlabbatch{1}.spm.stats.fmri_spec.timing.RT = TR;

% Run job
spm_jobman('run',matlabbatch);

cd(start_dir);
%% Import cortical surface

% Creates images: GLM/lh_surface.nii and GLM/rh_surface.nii
% and .mat files: GLM/lh_Srf.mat and GLM/rh_Srf.mat

% Structural image
struct = fullfile(data_dir,'T1.nii');

% Left hemisphere
spm_prf_import_surface(glm_dir, struct, surf_dir, 'lh');

% Right hemisphere
% spm_prf_import_surface(glm_dir, struct, surf_dir, 'rh');
%% Build a mask of voxels which survive p < 0.05
clear matlabbatch;
matlabbatch{1}.spm.stats.results.spmmat = cellstr(fullfile(glm_dir,'SPM.mat'));
matlabbatch{1}.spm.stats.results.conspec.titlestr = '';
matlabbatch{1}.spm.stats.results.conspec.contrasts = 1;
matlabbatch{1}.spm.stats.results.conspec.threshdesc = 'none';
matlabbatch{1}.spm.stats.results.conspec.thresh = 0.05;
matlabbatch{1}.spm.stats.results.conspec.extent = 0;
matlabbatch{1}.spm.stats.results.conspec.conjunction = 1;
matlabbatch{1}.spm.stats.results.conspec.mask.none = 1;
matlabbatch{1}.spm.stats.results.units = 1;
matlabbatch{1}.spm.stats.results.export{1}.binary.basename = 'mask_uncorrected';
spm_jobman('run',matlabbatch);
%% Remove voxels from the mask anterior to y = 25
cd(glm_dir);

% Read
V = spm_vol('spmF_0001_mask_uncorrected.nii');
[Y,XYZmm] = spm_read_vols(V);

% Threshold
i = XYZmm(2,:) > 25;

% Write
Y(i) = 0;
spm_write_vol(V,Y);

cd(start_dir);
%% Convert the postcentral gyrus label to a nifti mask so we can do some ROI analyses
label_file = fullfile(label_dir, 'lh.BA3b.label');
spm_prf_import_label(label_file, glm_dir);
%% Extract timeseries from surface voxels which survive p < 0.05

% Hemisphere
hemi = 'lh';

% Identify masks
spm_F_mask   = fullfile(glm_dir,'spmF_0001_mask_uncorrected.nii');
surface_mask = fullfile(glm_dir,'lh.BA3b.nii');

% Prepare batch
load('extract_timeseries_batch.mat');
matlabbatch{1}.spm.util.voi.name   = [hemi '_prf_mask'];
matlabbatch{1}.spm.util.voi.spmmat = cellstr(fullfile(glm_dir,'SPM.mat'));
matlabbatch{1}.spm.util.voi.roi{1}.mask.image = cellstr(spm_F_mask);
matlabbatch{1}.spm.util.voi.roi{2}.mask.image = cellstr(surface_mask);
matlabbatch{1}.spm.util.voi.expression = 'i1 & i2';

% Run batch
spm_jobman('run',matlabbatch);